#import <CoreFoundation/CoreFoundation.h>
#import <CoreServices/CoreServices.h>
#import <QuickLook/QuickLook.h>
#import <Cocoa/Cocoa.h>

/* -----------------------------------------------------------------------------
 Generate a preview for file
 
 This function's job is to create preview for designated file
 ----------------------------------------------------------------------------- */

NSData* processDC2(NSURL* url);

OSStatus GeneratePreviewForURL(void *thisInterface, QLPreviewRequestRef preview, CFURLRef url, CFStringRef contentTypeUTI, CFDictionaryRef options)
{
    
    CFDataRef previewData;
    
    previewData = (CFDataRef) processDC2((NSURL*) url);
    
    if (previewData) {
        
        CFDictionaryRef properties = (CFDictionaryRef) [NSDictionary dictionary];
        QLPreviewRequestSetDataRepresentation(preview, previewData, kUTTypeHTML, properties);
    }
    
    return noErr;
}


NSData* processDC2(NSURL* url)
{
    
    NSString *path2DC2 = [[NSBundle bundleWithIdentifier:@"net.delksterDC2.quicklook"] pathForResource:@"dc2tosvg" ofType:nil];
    
	NSTask* task = [[NSTask alloc] init];
	[task setLaunchPath: [path2DC2 stringByExpandingTildeInPath]];
	
    [task setArguments: [NSArray arrayWithObjects: nil]];
	
	NSPipe *writePipe = [NSPipe pipe];
	NSFileHandle *writeHandle = [writePipe fileHandleForWriting];
	[task setStandardInput: writePipe];
	
	NSPipe *readPipe = [NSPipe pipe];
	[task setStandardOutput:readPipe];
	
	[task launch];
    
    NSStringEncoding encoding = 0;
    
    // Ensure we used proper encoding - try different options until we get a hit
    //  if (plainText == nil)
    //    plainText = [NSString stringWithContentsOfFile:[url path] usedEncoding:<#(NSStringEncoding *)#> error:<#(NSError **)#> encoding:NSASCIIStringEncoding];
    

    
    NSString *theData = [NSString stringWithContentsOfFile:[url path] usedEncoding:&encoding error:nil];
    
	[writeHandle writeData:[theData dataUsingEncoding:NSUTF8StringEncoding]];
    
	[writeHandle closeFile];
	
	
	NSData *dc2Data = [[readPipe fileHandleForReading] readDataToEndOfFile];
    
    [task release];
	return dc2Data;
}

void CancelPreviewGeneration(void* thisInterface, QLPreviewRequestRef preview)
{
    // implement only if supported
}
