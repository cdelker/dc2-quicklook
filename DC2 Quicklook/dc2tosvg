#!/usr/bin/python
''' Convert DesignCad DC2 ASCII to SVG/HTML. '''

#-----------------------------------------------------------
# Copyright (c) 2014 Collin J. Delker,  www.collindelker.com
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met: 
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer. 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution. 
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#-----------------------------------------------------------

import fileinput

DEBUG = False
if DEBUG:
    import sys
    sys.stderr.write('Debug Mode ON\n')

# SVG pixel width/height. QL Has it defined as 800x600, so leave a little padding.
imgwidth = 780
imgheight = 580


def get_color( entity_hdr ):
    ''' Select color for entity. '''
    if len(entity_hdr) <= 9:
        # In some DC2 files, color is just a number 1-255. 
        # Don't know the real color translation table, but this works.
        colornum = int(entity_hdr[5])
        color = [(colornum&0xE0)*255/0xE0, (colornum&0x1C)*255/0x1C, (colornum&0x07)*255/0x07 ]
    else:
        # Otherwise, color is RGB value.
        color = [int(x) for x in entity_hdr[9:12]]

    # Don't allow white-on-white
    if color[0]==255 and color[1]==255 and color[2]==255: 
        color=[0,0,0]

    return color


if __name__ == '__main__':
    # Read the header line
    data = list( fileinput.input() )
    hdr = data[0].split()

    # Initialize some variables from the header
    xmin = float(hdr[0])
    xlen = float(hdr[2])
    ylen = float(hdr[3])
    ymin = -float(hdr[1]) - ylen        # y-coordinates are upside down from svg coordinates.
    min_line_width = xlen / imgwidth    # minimum line width depends on pixel size
    layer = 0


    # Print the HTML/SVG header
    print '<!DOCTYPE html>'
    print '<html>'
    print '<body>'
    print '<svg width="%dpx" height="%dpx" viewBox="%f %f %f %f">' % (imgwidth, imgheight, xmin, ymin, xlen, ylen )


    i = 1  # Already read the first row, so start on second.
    while i < len(data):
        entity_hdr = data[i].split()
        entity_type = entity_hdr[0]
    
        # Determine how many lines follow the entity header line. In most cases, the second
        # number in the entity line is the number of rows that follow.
        if entity_type=='21':            # Type 21 = Layer number. No lines follow.
            points=0
            layer = int(entity_hdr[1])
        elif entity_type=='20':          # Header of type 20 has either 3 lines or 13 lines, and ends with a "*" line.
            points=3
            if data[i+points][0] != '*':
                points = 13
        else:
            try:
                points = int(entity_hdr[1])
            except:
                # This isn't a header line. Maybe there was an extra data line previously?
                i = i + 1
                continue
            
        entity_data = data[i+1:i+points+1]

        if entity_type in ['42', '23', '20', '41']:    # 42, 20, 41 = header info. 23=Layer names
            pass

        elif entity_type=='21':                        # New Layer number
            layer = entity_hdr[1]

        elif entity_type == '13' or entity_type == '75':
            # Circles or dimension, have extra line of text. Ignore them for now.
            i=i+1

        elif entity_type =='3':                  # Text. Contains an extra line with string.
            color = get_color( entity_hdr )
            txt = data[i+points+1].strip()
            coords = entity_data[0].split()
            x = float(coords[0]); y=-float(coords[1])
            coords = entity_data[2].split()
            size = float(coords[1]) + y
            print '<text x="%f" y="%f" fill="rgb(%d,%d,%d)" font-size="%f">' % (x, y, color[0], color[1], color[2], size),
            print '%s</text>' % (txt)
            i=i+1

        elif entity_type=='1':                  # Line/Path
            # Make sure the line is wide enough we can see it.
            lw = float(entity_hdr[3])
            if lw == 0 or lw < min_line_width: lw = min_line_width
        
            color = get_color( entity_hdr )

            if points > 0:
                coords = entity_data[0].split()
                x = coords[0]; y=coords[1]          # Sometimes (but not always) there's also a Z coordinate on this line
                print '<path d="M%f %f ' % (float(x), -float(y)),   # y-coords are negative to get right orientation
            
                for p in entity_data[1:]:
                    coords = p.split()
                    x = float(coords[0]); y=-float(coords[1])
                    print 'L%f %f ' % (x, y),
                print '" stroke="rgb(%d,%d,%d)" stroke-width="%f"' % (color[0], color[1], color[2], lw),
                print 'fill="none" />'

        else:
            if DEBUG:
                sys.stderr.write( '-- Unsupported Entity Type %s\n' % entity_type )

        i = i+points+1

    # Finish up
    print '</svg>\n</body>\n</html>\n'
