DC2 QuickLook Generator
---------------------------------------------------------------------

This is a Mac OS X Quicklook generator for DC2 files, the ASCII format written by DesignCAD 2D.
To install, copy the "DC2 Quicklook.qlgenerator" file to /Library/QuickLook
(or ~/Library/QuickLook for single user use).

Limitations:
This is not meant to generate an accurate or complete rendition of the DC2 file. Several
drawing elements, such as dimensions and certain curve types, are not yet implemented.
The software is simply meant to provide a quick preview of the actual contents on an OS X system.

For additional info or to obtain the most current version, please visit:
http://www.collindelker.com/wp/2014/02/dc2-quicklook/

Source code is available from the git repository at:
https://bitbucket.org/cdelker/dc2-quicklook


-----------------------------------------------------------
Copyright (c) 2014 Collin J. Delker
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-----------------------------------------------------------

